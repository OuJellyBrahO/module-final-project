<?php

class DataAccess{
	
	private $link;

	/**
	* Constructor
	* @param $link 		The connection to the database
	*/
	function __construct($link){
		$this->link = $link;
	}


	/**
	* Get all tasks
	* @param $order_by 		Optional - the sort order (title or author)
	* @return 2d array 		Returns an array of tasks (each task is an assoc array)
	*/
	function get_all_tasks($order_by = null){
		// TODO: if the order_by param is not null we need to sort the tasks (should be by title or author)
		$qStr = "SELECT	id, taskName, description, myName, complete FROM to_do";
		/*
		if($order_by == "DESC"){
			$qStr .= " ORDER BY ";
		}
		*/

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		$all_tasks = array();

		while($row = mysqli_fetch_assoc($result)){

			$task = array();
			$task['id'] = htmlentities($row['id']);
			$task['taskName'] = htmlentities($row['taskName']);
			$task['description'] = htmlentities($row['description']);
			$task['myName'] = htmlentities($row['myName']);
			$task['complete'] = htmlentities($row['complete']);

			$all_tasks[] = $task;
		}

		return $all_tasks;
	}


	/**
	* Get a task by its ID
	* @param $id 			The ID of the task to get
	* @return array 		An assoc array that has keys for each property of the task
	*/
	function get_task_by_id($id){
		
		// TODO: if the order_by param is not null we need to sort the tasks (should be by title or author)
		$qStr = "SELECT	id, taskName, description, myName, complete FROM to_do WHERE id = " . mysqli_real_escape_string($this->link, $id);

				
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result->num_rows == 1){
			$row = mysqli_fetch_assoc($result);

			$task = array();
			$task['id'] = htmlentities($row['id']);
			$task['taskName'] = htmlentities($row['taskName']);
			$task['description'] = htmlentities($row['description']);
			$task['myName'] = htmlentities($row['myName']);
			$task['complete'] = htmlentities($row['complete']);

			$all_tasks[] = $task;

			return $all_tasks;

		}else{
			return null;
		}
			
	}


	function insert_task($task){

		// prevent SQL injection
		$task['taskName'] = mysqli_real_escape_string($this->link, $task['taskName']);
		$task['description'] = mysqli_real_escape_string($this->link, $task['description']);
		$task['myName'] = mysqli_real_escape_string($this->link, $task['myName']);
		$task['complete'] = mysqli_real_escape_string($this->link, $task['complete']);
		

		$qStr = "INSERT INTO to_do (
					taskName,
					description,
					myName,
					complete
				) VALUES (
					'{$task['taskName']}',
					'{$task['description']}',
					'{$task['myName']}',
					'{$task['complete']}'
				)";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			// add the task id that was assigned by the data base
			$task['id'] = mysqli_insert_id($this->link);
			// then return the task
			return $task;
		}else{
			$this->handle_error("unable to insert task");
		}

		return false;
	}



	function update_task($task){

		// prevent SQL injection
		$task['id'] = mysqli_real_escape_string($this->link, $task['id']);
		$task['taskName'] = mysqli_real_escape_string($this->link, $task['taskName']);
		$task['description'] = mysqli_real_escape_string($this->link, $task['description']);
		$task['myName'] = mysqli_real_escape_string($this->link, $task['myName']);
		$task['complete'] = mysqli_real_escape_string($this->link, $task['complete']);

		$qStr = "UPDATE to_do 
				SET id='{$task['id']}',
				taskName='{$task['taskName']}',
				description='{$task['description']}',
				myName='{$task['myName']}',
				complete='{$task['complete']}'
				WHERE id ='{$task['id']}'";

		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $task;
		}else{
			$this->handle_error("unable to update task");
		}

		return false;
	}
	


}