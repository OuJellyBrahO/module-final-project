<?php
class UserDataAccess{
	
	private $link;

	/**
	 * Constructor
	 *
	 * @param connection $link 	The link the the database 		
	 */
	function UserDataAccess($link){
		$this->link = $link;
	}

	/**
	* Authenticates a user for accessing the web site (or the control panel of the site)
	* 
	* @param string email
	* @param string password
	* 
	* @return array If login is authenticated returns the user object (as assoc array or User obj???).
	* 				Returns false if authentication fails (or if something goes wrong).
	*/
	function login($user, $pass){

		// Prevent SQL injection by scrubbing the email and password
		// with mysqli_real_escape_string();
		$username = mysqli_real_escape_string($this->link, $user);
		$password = mysqli_real_escape_string($this->link, $pass);

		$qStr = "SELECT
		userID, user_name, user_password, user_admin
		FROM users 
		WHERE user_name = '" . $username . "' AND user_password = '" . MD5($password) . "'";
		
		// this comes in really handy when you are testing your query....
		//die($qStr);

		// run the query and get a result object back
		// NOTE: Unlike the previous version of the code we are using the 'or' to call handle_error()
		// if something goes wrong with the query
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		// I see this a lot, but I don't agree with it:
		// $result = mysqli_query($this->link, $qStr) or die(mysqli_error($this->link));
		

		// ALSO new - we are treating $result as an obj with a num_rows property
		// instead of using the procedural function mysqli_num_rows($result)
		$num_rows = $result->num_rows;
		
		if($num_rows == 1){

			$row = mysqli_fetch_assoc($result);

			// scrub the data to prevent XSS attacks
			$user = array();
			$user['userID'] = htmlentities($row['userID']);
			$user['user_name'] = htmlentities($row['user_name']);
			$user['user_password'] = htmlentities($row['user_password']);
			$user['user_admin'] = htmlentities($row['user_admin']);
			

			return $user;
			
		}elseif($num_rows > 1){
			$this->handle_error("Duplicate email and passwords in DB!");
			return false;
		}else{
			return false;
		}
	}

	/**
	* Gets all users
	* 
	* @return array Returns an array of User objects??? 
	* 				Or an array of associative arrays???
	*/
	function get_all_staff(){
		$qStr = "SELECT
		userID, user_name, user_admin
		FROM users";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_users = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$user = array();
			$user['userID'] = htmlentities($row['userID']);
			$user['user_name'] = htmlentities($row['user_name']);
			$user['user_admin'] = htmlentities($row['user_admin']);


			// add the $user to the $all_users array
			$all_users[] = $user;
		}

		return $all_users;

	}

	function get_all_users(){
		$qStr = "SELECT
		client_id, client_fname, client_lname, client_email
		FROM client";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_users = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$user = array();
			$user['client_id'] = htmlentities($row['client_id']);
			$user['client_fname'] = htmlentities($row['client_fname']);
			$user['client_lname'] = htmlentities($row['client_lname']);
			$user['client_email'] = htmlentities($row['client_email']);


			// add the $user to the $all_users array
			$all_users[] = $user;
		}

		return $all_users;
	}

	function get_all_blasts(){
		$qStr = "SELECT
		blast_id, blast_from, blast_subject, blast_links, message, blast_timestamp, client_count, user_id
		FROM email_blast ORDER BY blast_id DESC";
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		$all_blasts = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$blasts = array();
			$blasts['blast_id'] = htmlentities($row['blast_id']);
			$blasts['blast_from'] = htmlentities($row['blast_from']);
			$blasts['blast_subject'] = htmlentities($row['blast_subject']);
			$blasts['blast_links'] = htmlentities($row['blast_links']);
			$blasts['message'] = htmlentities($row['message']);
			$blasts['blast_timestamp'] = htmlentities($row['blast_timestamp']);
			$blasts['client_count'] = htmlentities($row['client_count']);
			$blasts['user_id'] = htmlentities($row['user_id']);


			// add the $user to the $all_users array
			$all_blasts[] = $blasts;
		}

		return $all_blasts;

	}

	function get_unique_blasts($blastId){
		$qStr = "SELECT
		blast_id, blast_from, blast_subject, blast_links, message, blast_timestamp, client_count, user_id
		FROM email_blast WHERE blast_id = ".$blastId." ORDER BY blast_id DESC";
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		$all_blasts = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$blasts = array();
			$blasts['blast_id'] = htmlentities($row['blast_id']);
			$blasts['blast_from'] = htmlentities($row['blast_from']);
			$blasts['blast_subject'] = htmlentities($row['blast_subject']);
			$blasts['blast_links'] = htmlentities($row['blast_links']);
			$blasts['message'] = htmlentities($row['message']);
			$blasts['blast_timestamp'] = htmlentities($row['blast_timestamp']);
			$blasts['client_count'] = htmlentities($row['client_count']);
			$blasts['user_id'] = htmlentities($row['user_id']);


			// add the $user to the $all_users array
			$all_blasts[] = $blasts;
		}

		return $all_blasts;

	}

	function get_all_email_views(){
		$qStr = "SELECT
		blast_id, client_id, time_stamp
		FROM email_blast_views ORDER BY blast_id DESC";
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		$all_email_views = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$email = array();
			$email['blast_id'] = htmlentities($row['blast_id']);
			$email['client_id'] = htmlentities($row['client_id']);
			$email['time_stamp'] = htmlentities($row['time_stamp']);



			// add the $user to the $all_users array
			$all_email_views[] = $email;
		}

		return $all_email_views;

	}

	function get_unique_email_views($blastId){
		//die(var_dump($blastId));
		$qStr = "SELECT	blast_id, client_id, time_stamp FROM email_blast_views WHERE blast_id = ".$blastId;
		//die($qStr);
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		//die($result);

		$all_email_views = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$email = array();
			$email['blast_id'] = htmlentities($row['blast_id']);
			$email['client_id'] = htmlentities($row['client_id']);
			$email['time_stamp'] = htmlentities($row['time_stamp']);



			// add the $user to the $all_users array
			$all_email_views[] = $email;
		}

		return $all_email_views;

	}

	function get_all_landing_views(){
		$qStr = "SELECT
		blast_id, client_id, landing_page_url, time_stamp
		FROM landing_page_views ORDER BY blast_id DESC";
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		$all_landing_views = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$landing = array();
			$landing['blast_id'] = htmlentities($row['blast_id']);
			$landing['client_id'] = htmlentities($row['client_id']);
			$landing['landing_page_url'] = htmlentities($row['landing_page_url']);
			$landing['time_stamp'] = htmlentities($row['time_stamp']);



			// add the $user to the $all_users array
			$all_landing_views[] = $landing;
		}

		return $all_landing_views;

	}

	/**
	* Gets a user by the id that is passed in.
	*
	* @param number 	The id of he user to get
	*
	* @return array 	Returns an assoc array (or a User obj?)
	* 					Returns false if something goes wrong.
	*/
	function get_user_by_id($id){

		$qStr = "SELECT
		user_id, user_first_name, user_last_name, user_email, user_role, user_password, user_active
		FROM users
		WHERE user_id = " . mysqli_real_escape_string($this->link, $id);
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		if($result->num_rows == 1){

			$row = mysqli_fetch_assoc($result);

			$user = array();
			$user['user_id'] = htmlentities($row['user_id']);
			$user['user_first_name'] = htmlentities($row['user_first_name']);
			$user['user_last_name'] = htmlentities($row['user_last_name']);
			$user['user_email'] = htmlentities($row['user_email']);
			$user['user_role'] = htmlentities($row['user_role']);
			$user['user_password'] = htmlentities($row['user_password']);
			$user['user_active'] = htmlentities($row['user_active']);

			return $user;
			
		}else{
			$this->handle_error("something went wrong");
		}
	}

	function get_websites(){
		$qStr = "SELECT
		website_id, website_url, website_description
		FROM websites";
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		$all_websites = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$website = array();
			$website['website_id'] = htmlentities($row['website_id']);
			$website['website_url'] = htmlentities($row['website_url']);
			$website['website_description'] = htmlentities($row['website_description']);



			// add the $user to the $all_users array
			$all_websites[] = $website;
		}

		return $all_websites;
	}

	function get_unique_websites($websites){
		$ids = str_replace(","," OR website_id = ",$websites);
		$qStr = "SELECT
		website_id, website_url, website_description
		FROM websites WHERE website_id =".$ids." ";
		//return $qStr;
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		$all_websites = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$website = array();
			$website['website_id'] = htmlentities($row['website_id']);
			$website['website_url'] = htmlentities($row['website_url']);
			$website['website_description'] = htmlentities($row['website_description']);



			// add the $user to the $all_users array
			$all_websites[] = $website;
		}
		return $all_websites;
	}

	/**
	* Handles errors in UserDataAccess
	* 
	* @param array Returns an array of User objects??? Or an array of associative arrays???
	*/
	function handle_error($msg){
		// how do we want to handle this? should we throw an exception
		// and let our custom EXCEPTION handler deal with it?????
		$stack_trace = print_r(debug_backtrace(), true);
		throw new Exception($msg . " - " . $stack_trace);
	}

	// NOTE: we could make a DataAccess super class that has handle_error()
	// in it. Then we could sub class it and all sub classes could share the
	// same method (less code duplication)
}