window.addEventListener("load", function() {
  // Set up all the modules here...

  var tm = myNameSpace.TaskListModule.init();
  var da = acme.DataAccess();
  var tl = acme.TaskList.init();

  var target = tm.handleTargetDiv("tasklist-container");
  var parent = tm.createParentDiv(target);
  var leftDiv = tm.createLeftDiv(parent);
  var centerDiv = tm.createCenterDiv(parent);
  var rightDiv = tm.createRightDiv(parent);

  var activeTasks = [];
  var completeTasks = [];

  tm.fillLeftDiv(leftDiv, function(task) {
    if (task.id > 0) {
      da.putTask(task, function(response) {
        document.getElementById("txtId").value = -1;
        document.getElementById("txtName").value = "";
        document.getElementById("txtMyName").value = "";
        document.getElementById("txtdesc").value = "";
        document.getElementById("chkComplete").checked = false;
        centerDiv.innerHTML = "";
        rightDiv.innerHTML = "";
        da.getAllTasks(function(response) {
          var tasks = JSON.parse(response);
          activeTasks = [];
          completeTasks = [];
          for (var task in tasks) {
            if (tasks[task].complete == "0") {
              activeTasks.push(tasks[task]);
            }
            if (tasks[task].complete == "1") {
              completeTasks.push(tasks[task]);
            }
          }
          
		  completeTasks.reverse();
          tl.createList(centerDiv, activeTasks, "Active Taks", function(id) {
            da.getTaskById(id, function(callback) {
              tm.setTask(callback);
            });
          });
          tl.createList(rightDiv, completeTasks, "Completed Taks", function(
            id
          ) {
            
            da.getTaskById(id, function(callback) {
              tm.setTask(callback);
            });
          });
        });
      });
    } else {
      da.postTask(
        {
          taskName: task.taskName,
          description: task.description,
          myName: task.myName,
          complete: task.complete
        },
        function(response) {
          document.getElementById("txtId").value = -1;
          document.getElementById("txtName").value = "";
          document.getElementById("txtMyName").value = "";
          document.getElementById("txtdesc").value = "";
          document.getElementById("chkComplete").checked = false;
          centerDiv.innerHTML = "";
          rightDiv.innerHTML = "";
          da.getAllTasks(function(response) {
            var tasks = JSON.parse(response);
            activeTasks = [];
            completeTasks = [];
            for (var task in tasks) {
              if (tasks[task].complete == "0") {
                activeTasks.push(tasks[task]);
              }
              if (tasks[task].complete == "1") {
                completeTasks.push(tasks[task]);
              }
            }
			completeTasks.reverse();
            tl.createList(centerDiv, activeTasks, "Active Taks", function(id) {
              da.getTaskById(id, function(callback) {
                tm.setTask(callback);
              });
            });
            tl.createList(rightDiv, completeTasks, "Completed Taks", function(
              id
            ) {
              da.getTaskById(id, function(callback) {
                tm.setTask(callback);
              });
            });
          });
        }
      );
    }
  });

  da.getAllTasks(function(response) {
    var tasks = JSON.parse(response);
    activeTasks = [];
    completeTasks = [];
    for (var task in tasks) {
      if (tasks[task].complete == "0") {
        activeTasks.push(tasks[task]);
      }
      if (tasks[task].complete == "1") {
        completeTasks.push(tasks[task]);
      }
    }
	completeTasks.reverse();
    tl.createList(centerDiv, activeTasks, "Active Taks", function(id) {
      da.getTaskById(id, function(callback) {
        tm.setTask(callback);
      });
    });
    tl.createList(rightDiv, completeTasks, "Completed Taks", function(id) {
      da.getTaskById(id, function(callback) {
        tm.setTask(callback);
      });
    });
  });
});
