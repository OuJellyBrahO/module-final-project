/**
 * @module Tasklist 
 */

  /**
 * This Module does all the ajax calls to the database. 
 * @required This module depends on the acme.ajax module.
 * @class DataAccess
 */

var acme = acme || {};




acme.DataAccess = function() {
  var ajax = acme.ajax;
  var url = "/web-service/tasks/";
  if (!ajax) {
    throw new Error("This module depends on acme.ajax module");
  }
  /**
   * Method gets all tasks from the database
   * 
   * @method getAllTasks
   * @param {function} callback callback function if the ajax request is succesful
   */
  function getAllTasks(callback) {
    ajax.send({
      url: url,
      method: "GET",
      callback: callback
    });
  }
  /**
   * Method gets a task using its id value
   * 
   * @method getTaskById
   * @param {String} id id of the task
   * @param {function} callback callback function if the ajax request is succesful
   */
  function getTaskById(id, callback) {
    ajax.send({
      url: url + id,
      method: "GET",
      callback: callback
    });
  }

  /**
   * Method adds a new task to the database
   * 
   * @method postTask
   * @param {Object} task JSON task object
   * @param {function} callback callback function if the ajax request is succesful
   */
  function postTask(task, callback) {
    acme.ajax.send({
      url: url,
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data: JSON.stringify(task),
      callback: callback
    });
  }

  /**
   * Method updates a task in the database
   * 
   * @method putTask 
   * @param {Object} task JSON task object
   * @param {function} callback callback function if the ajax request is succesful
   */
  function putTask(task, callback) {
    acme.ajax.send({
      url: url + task.id,
      method: "POST",
      
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      
      data: JSON.stringify(task),
      callback: callback
    });
  }

  return {
    getAllTasks: getAllTasks,
    getTaskById: getTaskById,
    postTask: postTask,
    putTask: putTask
  };
};
