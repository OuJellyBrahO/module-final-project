/**
 * @module Tasklist
 * @main This package is a collection of modules used in adding a tasklist to your page
 */

 /**
 * This Module sets up the Tasklist module
 * @class TaskListModule
 */

var myNameSpace = myNameSpace || {};

myNameSpace.TaskListModule = {
  init: function() {
    /**
     * Checks to make sure the target DOM element exists.
     * 
     * @method handleTargetDiv
     * @param {String} id The id of the element to inject into.
     * @return {Object} Returns the DOM object if it exists
     */
    function handleTargetDiv(id) {
      var elem = document.getElementById(id);

      if (elem == null) {
        throw new Error("The id passed in does not match an element");
      }

      return elem;
    }
    /**
     * Creates the Parent Div that will contain the completed module
     * 
     * @method createParentDiv
     * @param {Object} elem  The DOM element this new element will inject into
     * @return {Object} Returns the div that was created
     */
    function createParentDiv(elem) {
      //CREATE PARENT DIV FOR ENTIRE MODULE//
      var div = document.createElement("div");
      div.id = "parentDiv";

      var title = document.createElement("h1");
      title.id = "parentTitle";
      
      title.innerHTML = "Task List Module";
      div.appendChild(title);
      elem.appendChild(div);

      return div;
    }

    /**
     * Creates the Left Div that will contain the completed module
     * 
     * @method createLeftDiv
     * @param {Object} elem  The DOM element this new element will inject into
     * @return {Object} Returns the div that was created
     */
    function createLeftDiv(div) {
      //CREATE LEFT DIV FOR ADD-EDIT
      var divLeft = document.createElement("div");
      divLeft.id = "divLeft";
      
      div.appendChild(divLeft);

      return divLeft;
    }
    /**
     * Fills the Left Div with the form used in creating tasks
     * 
     * @method fillLeftDiv
     * @param {Object} divLeft The Left Div of the Tasklist Module
     * @param {function} onSelectedCallback  The Callback function used if the form has been filled correctly
     */
    function fillLeftDiv(divLeft, onSelectedCallback) {
      var divTitle = document.createElement("h1");
      divTitle.id = "leftDivTitle";
      divTitle.innerHTML = "Add/Edit A Task<br>";
      
      divLeft.appendChild(divTitle);

      var txtId = document.createElement("input");
      txtId.setAttribute("type", "text");
      txtId.id = "txtId";
      txtId.value = -1;

      divLeft.appendChild(txtId);

      var nameLabel = document.createElement("h3");
      nameLabel.id = "nameLabel";
      nameLabel.innerHTML = "Task Name:";
      divLeft.appendChild(nameLabel);

      var nameLabelV = document.createElement("h5");
      nameLabelV.id = "nameLabelV";
      nameLabelV.innerHTML = "";
      divLeft.appendChild(nameLabelV);

      var txtName = document.createElement("input");
      txtName.setAttribute("type", "text");
      txtName.id = "txtName";
      divLeft.appendChild(txtName);

      var myNameLabel = document.createElement("h3");
      myNameLabel.id = "myNameLabel";
      myNameLabel.innerHTML = "Assign To:";
      divLeft.appendChild(myNameLabel);

      var myNameLabelV = document.createElement("h5");
      myNameLabelV.id = "myNameLabelV";
      myNameLabelV.innerHTML = "";
      divLeft.appendChild(myNameLabelV);

      var txtMyName = document.createElement("input");
      txtMyName.setAttribute("type", "text");
      txtMyName.id = "txtMyName";
      divLeft.appendChild(txtMyName);

      var descLabel = document.createElement("h3");
      descLabel.innerHTML = "Description:";
      descLabel.id = "descLabel";
      divLeft.appendChild(descLabel);

      var descLabelV = document.createElement("h5");
      descLabelV.id = "descLabelV";
      descLabelV.innerHTML = "";
      divLeft.appendChild(descLabelV);

      var txtdesc = document.createElement("textarea");
      txtdesc.id = "txtdesc";
      divLeft.appendChild(txtdesc);

      var completeLabel = document.createElement("h3");
      completeLabel.innerHTML = "Task Complete?";
      completeLabel.id = "completeLabel";
      divLeft.appendChild(completeLabel);

      var chkComplete = document.createElement("input");
      chkComplete.setAttribute("class", "onoffswitch-checkbox");
      chkComplete.name = "onoffswitch";
      chkComplete.setAttribute("type", "checkbox");
      chkComplete.id = "chkComplete";
      divLeft.appendChild(chkComplete);

      var onOffSwitchLabel = document.createElement("label");
      onOffSwitchLabel.setAttribute("for", "chkComplete");
      onOffSwitchLabel.setAttribute("class", "onoffswitch-label");

      var onOffSwitchInner = document.createElement("span");
      onOffSwitchInner.setAttribute("class", "onoffswitch-inner");      

      onOffSwitchLabel.appendChild(onOffSwitchInner);

      divLeft.appendChild(onOffSwitchLabel);

      var btnSave = document.createElement("input");
      btnSave.type = "button";
      btnSave.value = "Save";
      btnSave.id = "btnSave";
      

      btnSave.addEventListener("click", function(
        id,
        taskName,
        description,
        myName,
        complete
      ) {
        var txtId = document.getElementById("txtId").value;
        var txtName = document.getElementById("txtName").value;
        var txtMyName = document.getElementById("txtMyName").value;
        var txtdesc = document.getElementById("txtdesc").value;
        var chkComplete = document.getElementById("chkComplete").checked;
        var completeValue;
        if (chkComplete) {
          completeValue = "1";
        } else {
          completeValue = "0";
        }
        var nameLabelV = document.getElementById("nameLabelV");
        var myNameLabelV = document.getElementById("myNameLabelV");
        var descLabelV = document.getElementById("descLabelV");
        nameLabelV.innerHTML = "";
        myNameLabelV.innerHTML = "";
        descLabelV.innerHTML = "";
        if (txtName.trim() != "" && txtMyName.trim() != "" && txtdesc.trim() != "") {
          var task = {
            id: txtId,
            taskName: txtName,
            description: txtdesc,
            myName: txtMyName,
            complete: completeValue
          };
          onSelectedCallback(task);
        } else {
          if (txtName.trim() == "") {
            nameLabelV.innerHTML = "Please enter a Task Name";
            document.getElementById("txtName").focus();
          } else if (txtMyName.trim() == "") {
            myNameLabelV.innerHTML = "Please enter an assigned name";
            document.getElementById("txtMyName").focus();
          } else if (txtdesc.trim() == "") {
            descLabelV.innerHTML = "Please Enter a Description";
            document.getElementById("txtdesc").focus();
          }
        }
      });
      divLeft.appendChild(btnSave);

      var btnClear = document.createElement("input");
      btnClear.type = "button";
      btnClear.value = "Clear";
      btnClear.id = "btnClear";

      btnClear.addEventListener("click", function() {
        var nameLabelV = document.getElementById("nameLabelV");
        var myNameLabelV = document.getElementById("myNameLabelV");
        var descLabelV = document.getElementById("descLabelV");
        nameLabelV.innerHTML = "";
        myNameLabelV.innerHTML = "";
        descLabelV.innerHTML = "";
        document.getElementById("txtId").value = -1;
        document.getElementById("txtName").value = "";
        document.getElementById("txtMyName").value = "";
        document.getElementById("txtdesc").value = "";
        document.getElementById("chkComplete").checked = false;
      });
      divLeft.appendChild(btnClear);
    }

    /**
     * 
     * Creates the center DIV used to display all incomplete tasks.
     * @method createCenterDiv
     * @param {Object} div  The parent element that the center div will be injected into.
     */
    function createCenterDiv(div) {
      var divCenter = document.createElement("div");
      divCenter.id = "divCenter";
      div.appendChild(divCenter);

      return divCenter;
    }

    /**
     * Creates the right DIV that displays all completed task.
     * @method createRightDiv
     * @param {Object} div The parent element that the right div will be injected into.
     */
    function createRightDiv(div) {
      var divRight = document.createElement("div");
      divRight.id = "divRight";      
      div.appendChild(divRight);

      return divRight;
    }

    /**
     * Fills the left DIV with the task for editing
     * @method setTask
     * @param {Object} data 
     */
    function setTask(data) {
      var dataParse = JSON.parse(data);
      document.getElementById("txtId").value = dataParse[0].id;
      document.getElementById("txtName").value = dataParse[0].taskName;
      document.getElementById("txtMyName").value = dataParse[0].myName;
      document.getElementById("txtdesc").value = dataParse[0].description;
      if (dataParse[0].complete == "0") {
        document.getElementById("chkComplete").checked = false;
      } else {
        document.getElementById("chkComplete").checked = true;
      }
    }

    // RETURN THE PUBLIC API OF YOUR MODULE
    return {
      handleTargetDiv: handleTargetDiv,
      createParentDiv: createParentDiv,
      createLeftDiv: createLeftDiv,
      fillLeftDiv: fillLeftDiv,
      createCenterDiv: createCenterDiv,
      createRightDiv: createRightDiv,
      setTask: setTask
    };
  }
};
