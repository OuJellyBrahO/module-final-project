/**
 * 
 * @module Tasklist
 * 
 */



/**
 * @class Tasklist
 */


var acme = acme || {};



acme.TaskList = {
  init: function() {
    /**
     * Creates a list from the passed in array of tasks
     * 
     * @method createList
     * 
     * @param {Object} target       The target element the list will be injected into. 
     * @param {Array} data      The array of task data that will be up in the list 
     * @param {String} whichList        Name of the list for the title, this can be 'Active Tasks' or 'Completed Tasks'
     * @param {function} onSelectedCallback         The callback function if action succesful
     */
    function createList(target, data, whichList, onSelectedCallback) {

      var parent = document.createElement("ul");
      parent.id="parentUl";

      var liTitle = document.createElement("li");
      liTitle.id = "liTitle";
      with (liTitle) {
        
      }
      liTitle.innerHTML = "List of " + whichList;
      parent.appendChild(liTitle);
      //console.log(data);

      for (var i = 0; i < data.length; i++) {
        var li = document.createElement("li");
        li.setAttribute("class", "list-object");
        
        var table = document.createElement("table");
        table.setAttribute("class", "list-table");
        table.innerHTML = " \
            <tr> \
                <td style='font-size : 15px' valign = 'top' align = 'right'>Task</td> \
                <td>"+data[i]["taskName"]+"</td> \
            </tr> \
            <tr> \
                <td style='font-size : 15px' valign = 'top' align = 'right'>Assigned</td> \
                <td>"+data[i]["myName"]+"</td> \
            </tr> \
            <tr> \
                <td style='font-size : 15px' valign = 'top' align = 'right'>Description</td> \
                <td>"+data[i]["description"]+"</td> \
            </tr>";   
        
        li.appendChild(table);
        var btn = document.createElement("input");
        btn.setAttribute("type", "button");
        btn.setAttribute("value", "Edit");
        btn.setAttribute("class", "list-btn");
        btn.id = data[i]["id"];
        btn.addEventListener("click", function() {
          onSelectedCallback(this.id);
        });

        li.appendChild(btn);
        parent.appendChild(li);
      }
      target.appendChild(parent);
    }

    

    // Return the public API
    return {
      createList: createList
    };
  }
};
