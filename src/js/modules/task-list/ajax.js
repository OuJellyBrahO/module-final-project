/**
 * @module Tasklist
 */


 /**
  * @class ajax
  */

var acme = acme || {};

/**
    * Sends an XMLHttpRequest and returns the response in a callback.
    *
    * @method send
    * @param {Function} options.callback     This function will be triggers upon success. It will pass in the response as a param
    * @param {string} options.url		Url the XMLHttpRequest will call against
    * @param {Object} options.method	Method of the request
    * @param {Object} [options.headers=null]		Any headers added to the request. This is Optional
	* @param {Function} [options.errorCallback=null]	A function to call if there is an error with the request
	* @param {Object} options.data		This data will be a JSON parsed task object
*/

acme.ajax = {
	send: function(options){

		var http = new XMLHttpRequest();
		var callback = options.callback;
		var url = options.url;
		var errorCallback = options.errorCallback;
		var method = options.method;
		var headers = options.headers;
		var data = options.data;

		http.open(method, url);

		if(headers){
			for(var key in headers){
				http.setRequestHeader(key, headers[key]);
			}
		}

		http.addEventListener('readystatechange', function(){
			if(http.readyState == 4 && http.status == 200){
				callback(http.responseText);
			}else if(http.readyState == 4 && http.status != 200){
				if(errorCallback){
					errorCallback(http.status);
				}else{
					alert(http.status);
				}
			}
		});
		http.send(data);
		
		

	}
};