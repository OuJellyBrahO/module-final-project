YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "DataAccess",
        "TaskListModule",
        "Tasklist",
        "ajax"
    ],
    "modules": [
        "Tasklist"
    ],
    "allModules": [
        {
            "displayName": "Tasklist",
            "name": "Tasklist"
        }
    ],
    "elements": []
} };
});